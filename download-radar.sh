#!/bin/bash
curl -H 'Authorization: Bearer keyOVyuDF3w53gdvG' \
  https://api.airtable.com/v0/appkztexpd9BthLwv/Data \
  -G \
  -d view=Radar%20Export \
  -d fields=ID \
  -d fields=Name \
  -d fields=Ring \
  -d fields=Category \
  -d fields=isNew \
  -d fields=Description \
  -d fields=Radar \
  > data/radar.json

curl -H 'Authorization: Bearer keyOVyuDF3w53gdvG' \
  https://api.airtable.com/v0/appkztexpd9BthLwv/CategoryMapping \
  > data/categoryMapping.json

curl -H 'Authorization: Bearer keyOVyuDF3w53gdvG' \
  https://api.airtable.com/v0/appkztexpd9BthLwv/Radars \
  -G \
  -d fields=ID \
  -d fields=Name \
  > data/radars.json
