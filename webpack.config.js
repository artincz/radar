'use strict'

const webpack = require('webpack')
const path = require('path')
const buildPath = path.join(__dirname, './dist')
const args = require('yargs').argv

const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require("copy-webpack-plugin")

const isProd = args.prod
const isDev = args.dev
const env = args.envFile
if (env) {
  // Load env file
  require('dotenv').config({ path: env })
}

const main = ['./src/site.js']
const common = ['./src/common.js']
let devtool

if (isDev) {
  main.push('webpack-dev-server/client?http://0.0.0.0:8080')
  devtool = 'source-map'
}

const plugins = [
  new MiniCssExtractPlugin(),
  new HtmlWebpackPlugin({
    template: './src/index.html',
    chunks: ['main'],
    inject: 'body',
  }),
  new HtmlWebpackPlugin({
    template: './src/error.html',
    chunks: ['common'],
    inject: 'body',
    filename: 'error.html',
  }),
  new CopyPlugin({
    patterns: [
      // '**/*.csv',
       '**/*.json'
    ]
  }),
]

if (isProd) {
  plugins.push(new webpack.NoEmitOnErrorsPlugin())
}

module.exports = {
  entry: {
    main: main,
    common: common,
  },
  resolve: {
    fallback: {
      fs: false,
      net: false,
      tls: false,
      child_process: false,
    }
  },

  output: {
    path: buildPath,
    publicPath: '/',
    filename: '[name].[hash].js',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{ loader: 'babel-loader' }],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { importLoaders: 1, modules: 'global', url: false },
          },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'sass-loader',
            options: {
              implementation: require('sass')
            },
          }
        ],
      },
      {
        test: /\.(eot|svg|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      },
      {
        test: /\.(png|jpg|ico|gif|ttf)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              context: './src'
            }
          },
        ],
      },
      {
        test: require.resolve('jquery'),
        use: [
          { loader: 'expose-loader', options: { exposes: ['jQuery', '$'] } },
        ],
      },
    ],
  },

  plugins: plugins,

  devtool: devtool,

  devServer: {
    static: buildPath,
    host: '0.0.0.0',
    port: 8080,
  },
}
