FROM node:16 as build
WORKDIR /home/node
COPY package*.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM nginx:1.21
COPY --from=build /home/node/dist /usr/share/nginx/html
