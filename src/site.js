require('./common');
require('./images/logo.png');
require('./images/radar_legend.png');

const d3 = require('d3')
const _ = {
  map: require('lodash/map'),
  keyBy: require('lodash/keyBy'),
  head: require('lodash/head'),
  filter: require('lodash/filter'),
  fromPairs: require('lodash/fromPairs'),
}

const factory = require('./util/factory');

const GITLAB_TOKEN = 'glpat-tsFZxQVxdkcaZzLB52FH';

async function main() {
  const params = new URLSearchParams(window.location.search);
  let radar = params.get('radar') || 'main';
  let ref = params.get('branch');

  if (ref) {
    const url = `https://git.artin.cz/api/v4/projects/radar%2Fradar-content/jobs/artifacts/${ref}/raw/out/${radar}.json?job=create`;
    factory.ArtinJson(url, { token: GITLAB_TOKEN, ref }).init().build();
  } else {
    factory.ArtinJson(`data/${radar}.json`).init().build();
  }
}

main()
