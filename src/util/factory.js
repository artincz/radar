/* eslint no-constant-condition: "off" */

const d3 = require('d3')
const _ = {
  map: require('lodash/map'),
  uniqBy: require('lodash/uniqBy'),
  capitalize: require('lodash/capitalize'),
  each: require('lodash/each'),
  assign: require('lodash/assign'),
  keyBy: require('lodash/keyBy'),
  head: require('lodash/head'),
  filter: require('lodash/filter'),
  find: require('lodash/find'),
}

const InputSanitizer = require('./inputSanitizer')
const Radar = require('../models/radar')
const Quadrant = require('../models/quadrant')
const Ring = require('../models/ring')
const Blip = require('../models/blip')
const GraphingRadar = require('../graphing/radar')
const MalformedDataError = require('../exceptions/malformedDataError')
const SheetNotFoundError = require('../exceptions/sheetNotFoundError')
const ContentValidator = require('./contentValidator')
const ExceptionMessages = require('./exceptionMessages')

const plotRadar = function (title, blips, currentRadarName, alternativeRadars) {
  if (title.endsWith('.csv')) {
    title = title.substring(0, title.length - 4)
  }
  if (title.endsWith('.json')) {
    title = title.substring(0, title.length - 5)
  }
  document.title = title
  d3.selectAll('.loading').remove()

  var rings = _.map(_.uniqBy(blips, 'ring'), 'ring')
  var ringMap = {}
  var maxRings = 4

  _.each(rings, function (ringName, i) {
    if (i === maxRings) {
      throw new MalformedDataError(ExceptionMessages.TOO_MANY_RINGS)
    }
    ringMap[ringName] = new Ring(ringName, i)
  })

  var quadrants = {}
  _.each(blips, function (blip) {
    if (!quadrants[blip.quadrant]) {
      quadrants[blip.quadrant] = new Quadrant(_.capitalize(blip.quadrant))
    }
    quadrants[blip.quadrant].add(
      new Blip(blip.name, ringMap[blip.ring], blip.isNew.toLowerCase() === 'true', blip.topic, blip.description),
    )
  })

  var radar = new Radar()
  _.each(quadrants, function (quadrant) {
    radar.addQuadrant(quadrant)
  })

  if (alternativeRadars !== undefined || true) {
    alternativeRadars.forEach(function (sheetName) {
      radar.addAlternative(sheetName)
    })
  }

  if (currentRadarName !== undefined || true) {
    radar.setCurrentSheet(currentRadarName)
  }

  var size = window.innerHeight - 133 < 620 ? 620 : window.innerHeight - 133

  new GraphingRadar(size, radar).init().plot()
}

const CSVDocument = function (url) {
  var self = {}

  self.build = function () {
    d3.csv(url).then(createBlips)
  }

  var createBlips = function (data) {
    try {
      var columnNames = data.columns
      delete data.columns
      var contentValidator = new ContentValidator(columnNames)
      contentValidator.verifyContent()
      contentValidator.verifyHeaders()
      var blips = _.map(data, new InputSanitizer().sanitize)
      plotRadar(FileName(url), blips, 'CSV File', [])
    } catch (exception) {
      plotErrorMessage(exception)
    }
  }

  self.init = function () {
    plotLoading()
    return self
  }

  return self
}


const Airtable = function(data, radar, title, categoryMapping) {
  var self = {}

  self.build = async function() {
    await createBlips(data);
  }

  var getQuadrant = function(category) {
    const r = _.find(categoryMapping, r => r.radar === radar && r.category === category)
    if (r) {
      return r.quadrant
    }
    return _.find(categoryMapping, r => r.radar === 'main' && r.category === category).quadrant
  }

  var createBlips = function(data) {
    try {
      let blips = _.filter(data.records, row => (row.fields['Radar'] || []).includes(radar))
      blips = _.map(blips, row => {
        const category = _.head(row.fields.Category) || 'default';
        const quadrant = getQuadrant(category);

        return {
          name: row.fields.Name || row.fields.ID,
          ring: row.fields.Ring.join(', '),
          quadrant,
          description: row.fields.Description,
          isNew: (row.fields.isNew || false).toString(),
        };
      })
      blips = _.map(blips, new InputSanitizer().sanitize)

      plotRadar(title, blips, radar, [])
    } catch (exception) {
      console.error(exception)
      plotErrorMessage(exception)
    }
  }

  self.init = function () {
    plotLoading()
    return self
  }

  return self
}

const ArtinJson = function(url, opts) {
  var self = {}

  self.build = function() {
    let reqOpts = {};
    if (opts && opts.token) {
      reqOpts.headers = { 'PRIVATE-TOKEN': opts.token };
    }
    d3.json(url, reqOpts).then(createBlips)
  }

  var createBlips = function(data) {
    try {
      var blips = _.map(data.items, new InputSanitizer().sanitize)
      plotRadar(data.radar.title, blips, data.radar.title, [])
    } catch (exception) {
      plotErrorMessage(exception)
    }
  }

  self.init = function () {
    plotLoading()
    return self
  }

  return self
}

const JSONFile = function (url) {
  var self = {}

  self.build = function () {
    d3.json(url).then(createBlips)
  }

  var createBlips = function (data) {
    try {
      var columnNames = Object.keys(data[0])
      var contentValidator = new ContentValidator(columnNames)
      contentValidator.verifyContent()
      contentValidator.verifyHeaders()
      var blips = _.map(data, new InputSanitizer().sanitize)
      plotRadar(FileName(url), blips, 'JSON File', [])
    } catch (exception) {
      plotErrorMessage(exception)
    }
  }

  self.init = function () {
    plotLoading()
    return self
  }

  return self
}

const FileName = function (url) {
  var search = /([^\\/]+)$/
  var match = search.exec(decodeURIComponent(url.replace(/\+/g, ' ')))
  if (match != null) {
    var str = match[1]
    return str
  }
  return url
}

function setDocumentTitle () {
  document.title = 'Technology Radar'
}

function plotLoading (content) {
  content = d3
    .select('body')
    .append('div')
    .attr('class', 'loading')
    .append('div')
    .attr('class', 'input-sheet')

  setDocumentTitle()

  plotLogo(content)

  var bannerText = '<h1>Building your radar...</h1><p>Your Technology Radar will be available in just a few seconds</p>'
  plotBanner(content, bannerText)
  plotFooter(content)
}

function plotLogo (content) {
  content
    .append('div')
    .attr('class', 'input-sheet__logo')
    .html('<a href="https://www.artin.cz"><img src="/images/tw-logo.png" / ></a>')
}

function plotFooter (content) {
  content
    .append('div')
    .attr('id', 'footer')
    .append('div')
    .attr('class', 'footer-content')
    .append('p')
    .html('<a class="artin-link" href="https://www.artin.eu">&gt; artin.eu</a>')
}

function plotBanner (content, text) {
  content
    .append('div')
    .attr('class', 'input-sheet__banner')
    .html(text)
}

function plotErrorMessage (exception) {
  var message = 'Oops! It seems like there are some problems with loading your data. '

  var content = d3
    .select('body')
    .append('div')
    .attr('class', 'input-sheet')
  setDocumentTitle()

  plotLogo(content)

  var bannerText = ''

  plotBanner(content, bannerText)

  d3.selectAll('.loading').remove()
  message = "Oops! We can't find the Google Sheet you've entered"
  var faqMessage = ''
  if (exception instanceof MalformedDataError) {
    message = message.concat(exception.message)
  } else if (exception instanceof SheetNotFoundError) {
    message = exception.message
  } else {
    console.error(exception)
  }

  const container = content.append('div').attr('class', 'error-container')
  var errorContainer = container.append('div').attr('class', 'error-container__message')
  errorContainer
    .append('div')
    .append('p')
    .html(message)
  errorContainer
    .append('div')
    .append('p')
    .html(faqMessage)

  var homePageURL = window.location.protocol + '//' + window.location.hostname
  homePageURL += window.location.port === '' ? '' : ':' + window.location.port
  var homePage = '<a href=' + homePageURL + '>GO BACK</a>'

  errorContainer
    .append('div')
    .append('p')
    .html(homePage)

  plotFooter(content)
}

module.exports = {
  CSVDocument, JSONFile, Airtable, ArtinJson
}
